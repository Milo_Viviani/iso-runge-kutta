function [FF, VHf, K2]=build_vector_field_SL(h,A,s,X,Z,m1,m2,COR,GAMMA,J,NMAT,NV,eqn)

persistent A0 AA AA2 sA sA2

    if isempty(AA)
        
        A0 = zeros(s,s^2); 
        EYE = eye(s);

        for j=1:s
            
            A0(:,(j-1)*s+1:j*s) = kron(A(j,:),EYE(:,j));
        
        end
        
        AA = kron(A,eye(NMAT));
        AA2 = kron(kron(A,ones(s,1)),eye(NMAT));
        
        for i=1:s
        
            sA((i-1)*s*NMAT+1:i*s*NMAT,(i-1)*s*NMAT+1:i*s*NMAT) = AA;
    
        end
        
        sA2 = kron(A0,eye(NMAT));
        
    end
    
   %Inital values for the iteration 
   sX = repmat(X,s,1);
   Y1 = Z(1:s*NMAT,:);
   Y2 = Z(s*NMAT+1:2*s*NMAT,:);
   K = Z(2*s*NMAT+1:(s^2+2*s)*NMAT,:);
       
    %% Vector field

    %Y-part
    
    AY1 = AA*Y1;    
    sAY1 = AA2*Y1;
    
    AY2 = AA*Y2;   
    sAY2 = AA2*Y2;
    
    %K-part
    AK = sA*K;
    AK2 = sA2*K;

    %Hamiltonian argument
    SZ = sX + AY1 + AY2 + AK2;
    LW = grad_ham(SZ,COR,GAMMA,J,NV,NMAT,s,eqn);
    sLW = repmat(LW,s,1);

    %Vector field
    FY1 = -Y1 - h*m1(sX + AY1 ,LW); 
    FY2 = -Y2 + h*m2(LW,sX + AY2); 
    FK = -K + h*m2(sLW,sAY1 + AK);     

    FF = [FY1; FY2; FK];
 %% Hessian
    
    %Y-part
    AFY1 = AA*FY1;
    sAFY1 = AA2*FY1;
    
    AFY2 = AA*FY2;
    
    %K-part
    AFK = sA*FK;
    AFK2 = sA2*FK;
    
    %Hamiltonian argument
    SFF = AFY1 + AFY2 + AFK2;
    LWW =  hess_ham(SZ,SFF,J,NV,NMAT,s,eqn);
    sLWW = repmat(LWW,s,1);
    
    %Hessian
    VHfY1 = FY1 - h*(m1(sX + AY1 ,LWW)+m1(AFY1 ,LW)); 
    VHfY2 = FY2 + h*(m2(LWW,sX + AY2)+m2(LW,AFY2)); 
    VHfK = FK + h*(m2(sLWW,sAY1 + AK)+m2(sLW,sAFY1 + AFK));     
    VHf = [VHfY1; VHfY2; VHfK];
  
    K2 = -h*m1(sAY2 + AK,sLW);
end