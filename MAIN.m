%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                        %
%                        ISOSPECTRAL RUNGE-KUTTA                         %
%                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all

%% DEFAULT PARAMETERS
h = 1e-1;%Step of time discretization
NMAT = 10;%Dimension of matrices     
itermax = 10000; %Number of iterations

%% CHOICE OF THE EQUATION
fprintf('Choose an equation \n');
fprintf('1 = Vorticity Euler equations on the sphere \n');
fprintf('2 = Point vortices on a sphere \n');
fprintf('3 = Heisenberg spin chain \n');
fprintf('4 = Toeplitz inverse eigenvalue problem \n');
fprintf('5 = Periodic Toda flow \n');
fprintf('5.1 = Generalized Toda flow \n');
fprintf('6 = Rigid body \n');

eqn = input(' ');

if eqn~=1 && eqn~=2 && eqn~=3 && eqn~=4 && eqn~=5 && eqn~=5.1 && eqn~=6
    
    error('Equation not available')
    
end

if eqn~=2 && eqn~=3
    
    NV = 1;
    
else
    
switch eqn
    
    case 2
        
        fprintf('Choose number of point vortices \n');
    
    case 3
        
        fprintf('Choose number of spin particles \n');
        
end

    NV = input(' ');
    NMAT = 3;
    
end

%% CHOICE OF THE NUMERICAL SCHEME

fprintf('Choose numerical scheme \n');
fprintf('1 = Symplectic Runge-Kutta for (su(N)*)^NV (to modify for any quadratic Lie algebra it is enuogh to change the tra.m function) \n');
fprintf('2 = Symplectic Runge-Kutta for (sl(N,C)*)^NV \n');
fprintf('3 = Partitioned symplectic Runge-Kutta for (sl(N,C)*)^NV \n');

method = input(' ');


%% INTIAL VALUES

[X0,COR,GAMMA,J] = initial_values(NMAT,NV,eqn);
X = X0;
fprintf('N = %d',NMAT)
fprintf('\n');
fprintf('NV = %d',NV);
fprintf('\n');
fprintf('Time step %d',h);
fprintf('\n');

%% INITIAL HAMILTONIAN
H0 = hamiltonian(X0,COR,GAMMA,J,NV,NMAT,eqn);
VAR_HAM = zeros(1,itermax);

%% DIFF_EIG

DIFF_EIG = zeros(itermax,NMAT);

if eqn~=2 && eqn~=3
    
    E0 = sort(eig(X0));

end

%% COMPONENTS TOEPLITZ

COMP = zeros(itermax,NMAT^2);

%% DIFF_MOM

DIFF_MOM = zeros(itermax,3); 

if eqn==2 || eqn==3
    
    BIG_GAMMA = repelem(GAMMA',3);
    GX0 = BIG_GAMMA.*X0;
    VM0 = zeros(3,1);

    for i=1:NV
    
        VM0(1) = VM0(1) + GX0(1,3*(i-1)+2);
        VM0(2) = VM0(2) + GX0(1,3*(i-1)+3);
        VM0(3) = VM0(3) + GX0(2,3*(i-1)+3);
    
    end
end

for iter=1:itermax       
   
    %% HAMILTONIAN VARIATION 
   
   if eqn~=4 && eqn~=5 && eqn~=5.1
    
        H = hamiltonian(X,COR,GAMMA,J,NV,NMAT,eqn);
        VAR_HAM(iter) = abs(H-H0);
   
   end
   
   %% DIFF_EIG
   
   if eqn~=2 && eqn~=3
   
        DIFF_EIG(iter,:) = abs(sort(eig(X))-sort(E0));

   end
   
   
   %% DIFF_MOM
   
   if eqn==2 || eqn==3
       
   GX = BIG_GAMMA.*X;
   VM = zeros(3,1);

    for i=1:NV
    
        VM(1) = VM(1) + GX(1,3*(i-1)+2);
        VM(2) = VM(2) + GX(1,3*(i-1)+3);
        VM(3) = VM(3) + GX(2,3*(i-1)+3);
    
    end
    
    DIFF_MOM(iter,:) = abs(VM-VM0);
    
   end

    %% COMPONENTS TOEPLITZ
      
      if eqn==4
  
          COMP(iter,:) = X(1:end)'; 
      
      end

   %% NUMERICAL SCHEME
   
    switch method           
    
            case 1
                
               X = ISOSPECTRAL_RUNGE_KUTTA_QUADRATIC(h,X,COR,GAMMA,J,NV,NMAT,eqn);
               
            case 2
 
               X = ISOSPECTRAL_RUNGE_KUTTA_SL(h,X,COR,GAMMA,J,NV,NMAT,eqn);
               
            case 3
 
               X = ISOSPECTRAL_PARTITIONED_RUNGE_KUTTA(h,X,COR,GAMMA,J,NV,NMAT,eqn);
               
    end
   
end

%% PLOTS

plot_graphics(DIFF_EIG,DIFF_MOM,COMP,VAR_HAM,itermax,NMAT,h,eqn);