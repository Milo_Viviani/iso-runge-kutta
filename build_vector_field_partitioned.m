function [FF, VHf, K2]=build_vector_field_partitioned(h,A,A_2,s,X,Z,m1,m2,COR,GAMMA,J,NMAT,NV,eqn)

persistent AA AA2 A_2A A_2A2 sA_2 sA_22

    if isempty(AA)
        % Coefficient A
        AA = kron(A,eye(NMAT));
        AA2 = kron(kron(A,ones(s,1)),eye(NMAT));
        
        
        %Coefficecient A_2
        A_20 = zeros(s,s^2); 
        EYE = eye(s);
        
        for j=1:s
            
            A_20(:,(j-1)*s+1:j*s) = kron(A(j,:),EYE(:,j));
        
        end
        
        A_2A = kron(A_2,eye(NMAT));
        A_2A2 = kron(kron(A_2,ones(s,1)),eye(NMAT));
        
        for i=1:s
        
            sA_2((i-1)*s*NMAT+1:i*s*NMAT,(i-1)*s*NMAT+1:i*s*NMAT) = A_2A;
    
        end
        
        sA_22 = kron(A_20,eye(NMAT));
    end
    
   %Inital values for the iteration 
   sX = repmat(X,s,1);
   Y1 = Z(1:s*NMAT,:);
   Y2 = Z(s*NMAT+1:2*s*NMAT,:);
   K = Z(2*s*NMAT+1:(s^2+2*s)*NMAT,:);
       
    %% Vector field

    %Y-part
    
    AY1 = AA*Y1;    
    sAY1 = AA2*Y1;
    
    AY2 = A_2A*Y2;   
    sAY2 = A_2A2*Y2;
    
    %K-part
    AK = sA_2*K;
    AK2 = sA_22*K;

    %Hamiltonian argument
    SZ = sX + AY1 + AY2 + AK2;
    LW = grad_ham(SZ,COR,GAMMA,J,NV,NMAT,s,eqn);
    sLW = repmat(LW,s,1);

    %Vector field
    FY1 = -Y1 - h*m1(sX + AY1 ,LW); 
    FY2 = -Y2 + h*m2(LW,sX + AY2); 
    FK = -K + h*m2(sLW,sAY1 + AK);     

    FF = [FY1; FY2; FK];
 %% Hessian
    
    %Y-part
    AFY1 = AA*FY1;
    sAFY1 = AA2*FY1;
    
    AFY2 = A_2A*FY2;
    
    %K-part
    AFK = sA_2*FK;
    AFK2 = sA_22*FK;
    
    %Hamiltonian argument
    SFF = AFY1 + AFY2 + AFK2;
    LWW =  hess_ham(SZ,SFF,J,NV,NMAT,s,eqn);
    sLWW = repmat(LWW,s,1);
    
    %Hessian
    VHfY1 = FY1 - h*(m1(sX + AY1 ,LWW)+m1(AFY1 ,LW)); 
    VHfY2 = FY2 + h*(m2(LWW,sX + AY2)+m2(LW,AFY2)); 
    VHfK = FK + h*(m2(sLWW,sAY1 + AK)+m2(sLW,sAFY1 + AFK));     
    VHf = [VHfY1; VHfY2; VHfK];
  
    K2 = -h*m1(sAY2 + AK,sLW);
end