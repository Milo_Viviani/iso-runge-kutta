function [A,A_2,s,B,IV] = order_method_partitioned(order,NMAT,NV)

  
         switch order
            
            case 1
                
                 %1th order symplectic Euler
                 A = 1;
                 b = 1;
                        
                 A_2 = 0;
    
            case 2
        
                %2th St�rmer-Verlet
                 A = [0 0; 1/2 1/2];
                 b = [1/2 1/2];
                 
                 A_2 = [1/2 0; 1/2 0];
                 
             otherwise
            error('no method');
        end
                  
        %Common parameter
         s = length(b);
         B = kron(b,eye(NMAT));
         
    IV = zeros((2*s+s^2)*NMAT,NV*NMAT);        
        
end