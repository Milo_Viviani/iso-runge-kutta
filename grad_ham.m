    function LW=grad_ham(SZ,COR,GAMMA,J,NV,NMAT,s,eqn)
    
LW = zeros(s*NMAT,NMAT*NV);

switch eqn
    
    case 1
        %disp('Vorticity Euler equations on the sphere')  
         
        for l=1:s

            LW(NMAT*(l-1)+1:NMAT*l,:) = discrete_lapl_inv(SZ(NMAT*(l-1)+1:NMAT*l,:)-COR,NMAT);
            LW(NMAT*(l-1)+1:NMAT*l,:) = (LW(NMAT*(l-1)+1:NMAT*l,:) - LW(NMAT*(l-1)+1:NMAT*l,:)')/2;
            
        end       
       
        
    case 2
        %disp('Point vortices on a sphere')
  
        for l=1:s
            for i=1:NV
                for j=1:NV

                    if  j~=i
                         LW(NMAT*(l-1)+1:NMAT*l,NMAT*(i-1)+1:NMAT*i) = LW(NMAT*(l-1)+1:NMAT*l,NMAT*(i-1)+1:NMAT*i) + GAMMA(j)*0.5*(SZ(NMAT*(l-1)+1:NMAT*l,NMAT*(j-1)+1:NMAT*j))/(1-0.5*(trace((SZ(NMAT*(l-1)+1:NMAT*l,NMAT*(j-1)+1:NMAT*j))'*(SZ(NMAT*(l-1)+1:NMAT*l,NMAT*(i-1)+1:NMAT*i)))));
                    end
    
                end
            end
        end
    
    case 3
        %disp('Heisenberg spin chain')
        
        for i=2:NV-1    
    
            LW(:,NMAT*(i-1)+1:NMAT*i) = LW(:,NMAT*(i-1)+1:NMAT*i) + (SZ(:,NMAT*(i-2)+1:NMAT*(i-1)) + SZ(:,NMAT*i+1:NMAT*(i+1)));

        end

        LW(:,1:NMAT) = LW(:,1:NMAT) + (SZ(:,NMAT*(1)+1:NMAT*(2)) + SZ(:,NMAT*(NV-1)+1:NMAT*(NV)));
        LW(:,NMAT*(NV-1)+1:NMAT*(NV)) = LW(:,NMAT*(NV-1)+1:NMAT*(NV))+ (SZ(:,NMAT*(NV-2)+1:NMAT*(NV-1)) + SZ(:,1:NMAT));
            
    
    case 4
        %disp('Toeplitz inverse eigenvalue problem')
        
         for l=1:s
     
             A = triu(circshift(SZ(NMAT*(l-1)+1:NMAT*l,:),[0 1])-circshift(SZ(NMAT*(l-1)+1:NMAT*l,:),[-1 0]),1); 
    
             %Skew symmetric vector field
             A = A-A';
             LW(NMAT*(l-1)+1:NMAT*l,:) = A;
    
            %Centrosymmetric vector field
            LW(NMAT*(l-1)+1:NMAT*l,:) = (J*A*J+A)/2;
        end

       
    case 5
        %disp('Periodic Toda flow')
        
        for l=1:s
     
            D1 = diag(SZ(NMAT*(l-1)+1:NMAT*l,:),1);
            D2 = -diag(SZ(NMAT*(l-1)+1:NMAT*l,:),NMAT-1);
     
            LW(NMAT*(l-1)+1:NMAT*l,:) = diag(D1,1)  - diag(D1,-1)- diag(D2,NMAT-1) + diag(D2,-NMAT+1);

        end
        
     case 5.1
        %disp('Generalized Toda flow')
        
        for l=1:s
     
            D = tril(SZ(NMAT*(l-1)+1:NMAT*l,:),-1);
     
            LW(NMAT*(l-1)+1:NMAT*l,:) = D - D';

        end
        
    case 6
        %disp('Rigid body')
       
        for l=1:s

            LW(NMAT*(l-1)+1:NMAT*l,:) = sylvester(J/2,J/2,SZ(NMAT*(l-1)+1:NMAT*l,:));
            LW(NMAT*(l-1)+1:NMAT*l,:) = (LW(NMAT*(l-1)+1:NMAT*l,:) - LW(NMAT*(l-1)+1:NMAT*l,:)')/2;
        
        end
        
    otherwise
        error('no equation');
end 
 