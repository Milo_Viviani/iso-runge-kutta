function A = tra(A,NV,NMAT,eqn)

persistent SGN

if isempty(SGN)
    
    if eqn==1 || eqn==2 || eqn==3 || eqn==6
        
        SGN = -1;
    
    else

        SGN = 1;
        
    end
    
end

for i=1:NV
    
    for j=1:(size(A,1)/NMAT)
         
    %Here SGN + o - if S=Sym or S=su(N)    
    A(NMAT*(j-1)+1:NMAT*j,NMAT*(i-1)+1:NMAT*i) = SGN*A(NMAT*(j-1)+1:NMAT*j,NMAT*(i-1)+1:NMAT*i)';
    
    end
    
end
