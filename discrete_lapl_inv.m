function DLI=discrete_lapl_inv(mat,N)
    
%   persistent LI
% 
%   if isempty(LI) 
%       
%       IL = create_inv_lapl(N);
%       LI = sparse(IL);
%       
%   end
%  
%  TEMP = LI*mat(1:end).';
%  
%  DLI = reshape(TEMP,[N N]);

   persistent L U
 
   if isempty(L) 

       LI = -sparse_laplacian(N); 
       [L,U] = lu(LI);
%       maxnnz = size(LI,1)^2;
%        disp(['Non-zeros for LI: ',num2str(nnz(LI)/maxnnz)])
%        disp(['Non-zeros for L: ',num2str(nnz(L)/maxnnz)])
%        disp(['Non-zeros for U: ',num2str(nnz(U)/maxnnz)])
       
   end


 Y=L\mat(1:end).';
 X=U\Y;
  
 DLI = reshape(X,[N N]);

%  DLI = [1 2 3;4 5 6; 7 8 -6]/norm([1 2 3;4 5 6; 7 8 -6]);
%  
%  DLI = DLI-DLI';
 
% persistent IL
% 
%   if isempty(IL)
%       
%       IL = rand(N,N)+1i*rand(N,N);
%       IL = (IL-IL')-trace(IL-IL')*eye(N)/N;
%       IL = IL/norm(IL);
%   
%   end
% 
% DLI = IL;

end