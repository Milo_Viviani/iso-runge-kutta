%%Lie-Poisson Runge-Kutta scheme for (sl(NMAT)*)^NV

function X=ISOSPECTRAL_PARTITIONED_RUNGE_KUTTA(h,X,COR,GAMMA,J,NV,NMAT,eqn)

persistent A A_2 B s IV tol

    if isempty(A)
        
        order = 1;
        tol = 1e-15;
        
        fprintf('Order of the method %d',order);
        fprintf('\n');
        fprintf('Tolerance of the Newton iteration %d',tol);
        
        [A,A_2,s,B,IV] = order_method_partitioned(order,NMAT,NV);
        
    end
        
      

%Multiplication functions (shall these go into persistent?)
  m1 = @(A,B) mu1(A,B,NV,NMAT);
  m2 = @(A,B) mu2(A,B,NV,NMAT);
  vector_field = @(Z) build_vector_field_partitioned(h,A,A_2,s,X,Z,m1,m2,COR,GAMMA,J,NMAT,NV,eqn);
  
%Convergence parameters                                
 count = 0;
 err = 1;

%Initial values
 Z = IV;

%Newton method
while err>tol
     
   [FF, VHf, ~] = vector_field(Z);
   
   %Stopping criteria
   err = norm(FF);
      
   count=count+1;
   if count>1e4
       error('low precision');
   end
   
  %Method
      
   Z1 = Z + VHf;
   
   Z = Z1;
  

end 

[~, ~, K2]=vector_field(Z);

for i=1:s
    
    Z((i-1)*NMAT+1:i*NMAT,:) = Z((i-1)*NMAT+1:i*NMAT,:) + Z(s*NMAT+(i-1)*NMAT+1:s*NMAT+i*NMAT,:) + Z((i-1)*(s+1)*NMAT+2*s*NMAT+1:(i-1)*(s+1)*NMAT+2*s*NMAT+NMAT,:) + K2((i-1)*(s+1)*NMAT+1:(i-1)*(s+1)*NMAT+NMAT,:);

end

X = X + B*Z(1:s*NMAT,:);

end