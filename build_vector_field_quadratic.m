function [FF, VHf, SZ, LW]=build_vector_field_quadratic(h,A,s,X,Z,m1,m2,t,COR,GAMMA,J,NMAT,NV,eqn)

persistent A0 AA AA2 sA sA2

    if isempty(AA)
        
        A0 = zeros(s,s^2); 
        EYE = eye(s);

        for j=1:s
            
            A0(:,(j-1)*s+1:j*s) = kron(A(j,:),EYE(:,j));
        
        end
        
        AA = kron(A,eye(NMAT));
        AA2 = kron(kron(A,ones(s,1)),eye(NMAT));
        
        for i=1:s
        
            sA((i-1)*s*NMAT+1:i*s*NMAT,(i-1)*s*NMAT+1:i*s*NMAT) = AA;
    
        end
        
        sA2 = kron(A0,eye(NMAT));
        
    end
    
   %Inital values for the iteration 
   sX = repmat(X,s,1);
   Y = Z(1:s*NMAT,:);
   K = Z(s*NMAT+1:(s^2+s)*NMAT,:);
       
    %% Vector field

    %Y-part
    
    AY = AA*Y;
    AYt = AA*t(Y);    
    sAY = AA2*Y;
    
    %K-part
    AK = sA*K;
    AK2 = sA2*K;

    %Hamiltonian argument
    SZ = sX + AY + AYt + AK2;
    LW = grad_ham(SZ,COR,GAMMA,J,NV,NMAT,s,eqn);
    sLW = repmat(LW,s,1);

    %Vector field
    FY = -Y - h*m1(sX + AY ,LW); 
    FK = -K + h*m2(sLW,sAY + AK);  
   
    FF = [FY; FK];
    
 %% Hessian
    
    %Y-part
    AFY = AA*FY;
    AFYt = AA*t(FY);
    sAFY = AA2*FY;
    
    %K-part
    AFK = sA*FK;
    AFK2 = sA2*FK;
    
    %Hamiltonian argument
    SFF = AFY + AFYt + AFK2;
    LWW =  hess_ham(SZ,SFF,J,NV,NMAT,s,eqn);
    sLWW = repmat(LWW,s,1);
    
    %Hessian
     VHfY = FY - h*(m1(sX + AY ,LWW)+m1(AFY ,LW)); 
     VHfK = FK + h*(m2(sLWW,sAY + AK)+m2(sLW,sAFY + AFK));     

     VHf = [VHfY; VHfK];
  
end