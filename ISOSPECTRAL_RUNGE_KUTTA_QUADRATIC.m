%%Lie-Poisson Runge-Kutta scheme for (su(NMAT)*)^NV

function X=ISOSPECTRAL_RUNGE_KUTTA_QUADRATIC(h,X,COR,GAMMA,J,NV,NMAT,eqn)

persistent A B s IV tol


    if isempty(A)   
        
        order = 2;
        tol = 1e-15;
        
        fprintf('Order of the method %d',order);
        fprintf('\n');
        fprintf('Tolerance of the Newton iteration %d',tol);
        
        [A,s,B,IV] = order_method_quadratic(order,NMAT,NV);
    
    end


%Multiplication functions (shall these go into persistent?)
  t = @(mat) tra(mat,NV,NMAT,eqn);
  m1 = @(A,B) mu1(A,B,NV,NMAT);
  m2 = @(A,B) mu2(A,B,NV,NMAT);
  vector_field = @(Z) build_vector_field_quadratic(h,A,s,X,Z,m1,m2,t,COR,GAMMA,J,NMAT,NV,eqn);
  
%Convergence parameters                                
 count = 0;
 err = 1;

%Initial values
 Z = IV;

%Newton method
while err>tol
     
   [FF,VHf,~,~]=vector_field(Z);
   
   %Stopping criteria
   err = norm(FF);
      
   count=count+1;
   if count>1e4
       error('low precision');
   end
   
  %Method
      
   Z1 = Z + VHf;
   
   Z = Z1;
  

end 

for i=1:s
    
    Z((i-1)*NMAT+1:i*NMAT,:) = Z((i-1)*NMAT+1:i*NMAT,:) + Z((i-1)*(s+1)*NMAT+s*NMAT+1:(i-1)*(s+1)*NMAT+s*NMAT+NMAT,:);

end

X = X + B*(Z(1:s*NMAT,:)+t(Z(1:s*NMAT,:)));

end