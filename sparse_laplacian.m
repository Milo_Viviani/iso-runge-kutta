function DL=sparse_laplacian(N)

s = (N-1)/2;
k = s+1;

V = zeros(3*N^2-4*N+2+N,1);
I = V;
J = V;
count = 1;

for M1=-s:s
    for M2=-s:s     
        
        coeff1 = 2*(s*(s+1)-M1*M2);
        I(count) = (M1+k-1)*N + M2+k;
        J(count) = (M1+k-1)*N + M2+k;
        V(count) = -coeff1;
        count = count + 1;
        
        if M1<s && M2<s
        coeff2 = -sqrt(s*(s+1)-M1*(M1+1))*sqrt(s*(s+1)-M2*(M2+1));
        I(count) = (M1+k-1)*N + M2+k;
        J(count) = (M1+k)*N + M2+k+1;
        V(count) = -coeff2;
        count = count + 1;
        end
        
        if M1>-s && M2>-s
        coeff3 = -sqrt(s*(s+1)-M1*(M1-1))*sqrt(s*(s+1)-M2*(M2-1));
        I(count) = (M1+k-1)*N + M2+k;
        J(count) = (M1+k-2)*N + M2+k-1;
        V(count) = -coeff3;
        count = count + 1;
        end
        
    end
end

for h=0:N-1
        I(count) = 1;
        J(count) = 1+h*(N+1);
        V(count) = -1/N;
        count = count + 1;
end

DL = sparse(I,J,V,N^2,N^2);

end