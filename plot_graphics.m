function plot_graphics(DIFF_EIG,DIFF_MOM,COMP,VAR_HAM,itermax,NMAT,h,eqn)

%% PLOT EIGENVALUE VARIATION


if eqn~=2 && eqn~=3
 D = DIFF_EIG;
m = 10;
for j=1:NMAT
    for i=1:(itermax/m)
        D ((i-1)*m+1:i*m,j) = mean(DIFF_EIG((i-1)*m+1:i*m,j))*ones(m,1);
    end
end

    figure(1)
   
    plot(h*(1:itermax),D,'.-')
   title('Eigenvalues variation','fontsize',25)
   xlabel('Time','fontsize',25)
   ylabel('abs(eig(W)-eig(W0))','fontsize',25)
    set(gca, 'FontSize',25)

end

%% PLOT COMPONENT VARIATION

 if eqn==4
     
   figure(2)
   plot(h*(1:itermax),COMP,'.-')
   title('Components evolution','fontsize',25)
   xlabel('Time','fontsize',25)
   ylabel('W','fontsize',25)
   set(gca, 'FontSize',25)
   legend('W_{11}','W_{12}','W_{13}','W_{14}','W_{22}','W_{23}','W_{24}','W_{33}','W_{34}','W_{44}')

 end

%% PLOT MOMENTA VARIATION
 
 if eqn==2 || eqn==3
     
    figure(2)
    plot(h*(1:itermax),DIFF_MOM,'.-')
    title('Momentum variation','fontsize',25)
    xlabel('Time','fontsize',25)
    ylabel('abs(M(W)-M(W0))','fontsize',25)
    set(gca, 'FontSize',25)
    
 end

%% PLOT HAMILTONIAN VARIATION

if eqn~=4 && eqn~=5 && eqn~=5.1

    figure(3)
    title('Hamiltonian variation','fontsize',25)
    hold on
    xlabel('Time','fontsize',25)
    ylabel('abs(H(W)-H(W0))','fontsize',25)
    plot(h*(1:itermax),VAR_HAM,'.-');
    set(gca, 'FontSize',25)
end

end