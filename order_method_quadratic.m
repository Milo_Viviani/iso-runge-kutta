function [A,s,B,IV] = order_method_quadratic(order,NMAT,NV)

  
        switch order
            
            
    
            case 2
        
                %2th order Midpoint
                 A = 1/2;
                 b = 1;

         
            case 4
        
                 %4th order Gauss
                 A = [1/4 1/4-sqrt(3)/6;1/4+sqrt(3)/6 1/4];
                 b = [1/2 1/2];

            case 6
        
                 %6th order Gauss
                 A = [5/36 2/9-sqrt(15)/15 5/36-sqrt(15)/30;5/36+sqrt(15)/24 2/9 5/36-sqrt(15)/24;5/36+sqrt(15)/30 2/9+sqrt(15)/15 5/36];
                 b = [5/18 4/9 5/18];
         
            otherwise
            error('no method');
        end
         
        %Common parameter
         s = length(b);
         B = kron(b,eye(NMAT));
         
 IV = zeros((s+s^2)*NMAT,NV*NMAT);

end