function LWW=hess_ham(SZ,SFF,J,NV,NMAT,s,eqn)

LWW = zeros(s*NMAT,NMAT*NV);

switch eqn
    
    case 1
        %disp('Vorticity Euler equations on the sphere')
                    
        for l=1:s

            LWW(NMAT*(l-1)+1:NMAT*l,:) = discrete_lapl_inv(SFF(NMAT*(l-1)+1:NMAT*l,:),NMAT);
            LWW(NMAT*(l-1)+1:NMAT*l,:) = (LWW(NMAT*(l-1)+1:NMAT*l,:) - LWW(NMAT*(l-1)+1:NMAT*l,:)')/2;

        end   
        
    case 2
        %disp('Point vortices on a sphere')
 
        for l=1:s
            for i=1:NV
                for j=1:NV

                    if j~=i
        
                        LWW(NMAT*(l-1)+1:NMAT*l,NMAT*(i-1)+1:NMAT*i) = LWW(NMAT*(l-1)+1:NMAT*l,NMAT*(i-1)+1:NMAT*i) + (0.5)^2*(SZ(NMAT*(l-1)+1:NMAT*l,NMAT*(j-1)+1:NMAT*j))*trace((SZ(NMAT*(l-1)+1:NMAT*l,NMAT*(i-1)+1:NMAT*i))'*SFF(NMAT*(l-1)+1:NMAT*l,NMAT*(j-1)+1:NMAT*j))/(1-0.5*(trace((SZ(NMAT*(l-1)+1:NMAT*l,NMAT*(j-1)+1:NMAT*j))'*(SZ(NMAT*(l-1)+1:NMAT*l,NMAT*(i-1)+1:NMAT*i)))))^2 + 0.5*SFF(NMAT*(l-1)+1:NMAT*l,NMAT*(j-1)+1:NMAT*j)/(1-0.5*(trace((SZ(NMAT*(l-1)+1:NMAT*l,NMAT*(j-1)+1:NMAT*j))'*(SZ(NMAT*(l-1)+1:NMAT*l,NMAT*(i-1)+1:NMAT*i)))));

                    end
                end
            end
        end
    
    case 3
        %disp('Heisenberg spin chain')
        
        for i=2:NV-1

            LWW(:,NMAT*(i-1)+1:NMAT*i) = LWW(:,NMAT*(i-1)+1:NMAT*i) + (SFF(:,NMAT*(i-2)+1:NMAT*(i-1)) + SFF(:,NMAT*i+1:NMAT*(i+1)));
    
        end

        LWW(:,1:NMAT) = LWW(:,1:NMAT) + (SFF(:,NMAT*(1)+1:NMAT*(2)) + SFF(:,NMAT*(NV-1)+1:NMAT*(NV)));
        LWW(:,NMAT*(NV-1)+1:NMAT*(NV)) = LWW(:,NMAT*(NV-1)+1:NMAT*(NV))+ (SFF(:,NMAT*(NV-2)+1:NMAT*(NV-1)) + SFF(:,1:NMAT));            
    
    case 4
        %disp('Toeplitz inverse eigenvalue problem')
        
        for l=1:s
    
            A = triu(circshift(SFF(NMAT*(l-1)+1:NMAT*l,:),[0 1])-circshift(SFF(NMAT*(l-1)+1:NMAT*l,:),[-1 0]),1); 
    
            %Skew symmetric vector field
            A = A-A';
            LWW(NMAT*(l-1)+1:NMAT*l,:) = A;
    
            %Centrosymmetric vector field
            LWW(NMAT*(l-1)+1:NMAT*l,:) = (J*A*J+A)/2;
        end
       
    case 5
        %disp('Generalized Toda flow')
        
         for l=1:s
     
            D1 = diag(SFF(NMAT*(l-1)+1:NMAT*l,:),1);
            D2 = -diag(SFF(NMAT*(l-1)+1:NMAT*l,:),NMAT-1);
     
            LWW(NMAT*(l-1)+1:NMAT*l,:) = diag(D1,1)  -  diag(D1,-1) + diag(D2,NMAT-1) - diag(D2,-NMAT+1);

        end
        
    case 5.1
        %disp('Generalized Toda flow')
        
        for l=1:s
     
            D = tril(SFF(NMAT*(l-1)+1:NMAT*l,:),-1);
     
            LWW(NMAT*(l-1)+1:NMAT*l,:) = D - D';

        end
        
    case 6
        %disp('Rigid body')
 
        for l=1:s
         
              LWW(NMAT*(l-1)+1:NMAT*l,:) = sylvester(J/2,J/2,SFF(NMAT*(l-1)+1:NMAT*l,:));
              LWW(NMAT*(l-1)+1:NMAT*l,:) = (LWW(NMAT*(l-1)+1:NMAT*l,:) - LWW(NMAT*(l-1)+1:NMAT*l,:)')/2;
       
        end
        
    otherwise
        error('no equation');
end
 
end