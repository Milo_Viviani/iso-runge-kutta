function C = brak2(A,B,NV,NMAT)

%C = m22(A,B) - m12(B,A);

% persistent sizej
% 
% if isempty(sizej)
%     
    sizej = size(A,1)/NMAT;
    
%end

for i=1:NV
    
    for j=1:sizej
        
        C(NMAT*(j-1)+1:NMAT*j,NMAT*(i-1)+1:NMAT*i) = A(NMAT*(j-1)+1:NMAT*j,NMAT*(i-1)+1:NMAT*i)'*B(NMAT*(j-1)+1:NMAT*j,NMAT*(i-1)+1:NMAT*i)-B(NMAT*(j-1)+1:NMAT*j,NMAT*(i-1)+1:NMAT*i)*A(NMAT*(j-1)+1:NMAT*j,NMAT*(i-1)+1:NMAT*i)';
        
    end
        
end
