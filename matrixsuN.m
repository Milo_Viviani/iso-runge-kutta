%Matrix representation of su(N), N=2*M+1.

function T=matrixsuN(l,m,N)

path(path,'.\SH2');

     s = N/2;
    
if m>=0
    
    V = zeros(N+1-m,1);
    I = V;
    J = V;
    count = 1;

    for m1=-(s-m):s
    
        m2 = m1 - m; 
       
        w3j = wigner3jm(s,l,s,-m1,m,m2);
        WW = w3j(end);
        
        I(count) = m1+s+1;
        J(count) = m2+s+1;
        V(count) = N^(3/2)/(4*sqrt(pi))*(-1)^(s-m1)*sqrt(2*l+1)*WW; %N^1/2 is fine for SH approximation
        
        count = count + 1;
        
    end
    
else
    
    V = zeros(N+1+m,1);
    I = V;
    J = V;
    count = 1;
    
    for m1=-s:(s+m)
    
        m2 = m1 - m; 
        
        w3j = wigner3jm(s,l,s,-m1,m,m2);
        WW = w3j(end);
        
        I(count) = m1+s+1;
        J(count) = m2+s+1;
        V(count) = N^(3/2)/(4*sqrt(pi))*(-1)^(s-m1)*sqrt(2*l+1)*WW; %N^1/2 is fine for SH approximation
        
        count = count + 1;
        
    end
end
    
    
    T = sparse(I,J,V,N+1,N+1);

end