function [X0,COR,GAMMA,J] = initial_values(NMAT,NV,eqn)

X0 = zeros(NMAT,NMAT*NV);
COR = [];
GAMMA = [];
J = [];

switch eqn
    
    case 1
        disp('Vorticity Euler equations on the sphere')
        
        if NV~=1
            error('NV must be 1');
        end
   
       W0 = rand(NMAT,NMAT)+1i*rand(NMAT,NMAT);
       W0 = (W0-W0')-trace(W0-W0')*eye(NMAT)/NMAT;
       X0 = W0/norm(W0);
  
       rot = 'n';%Rotation of the sphere
        
        if rot == 'n'
        %No rotation of the sphere
            COR = zeros(NMAT);
        
        else
        %Rotation of the sphere
            Omega = 10; %Angular velocity of the Earth
            cor = -4*sqrt(pi/3)*Omega; %Coriolis parameter
            COR = cor*1i*matrixsuN(1,0,NMAT-1);
            COR = COR/norm(full(COR));
        end
        
    case 2
        disp('Point vortices on a sphere')
        GAMMA = zeros(NV,1);
        
        for i=1:NV
            
            W0 = rand(NMAT,NMAT);
            W0 = W0-W0';
            W0 = W0/norm(W0);
            X0(:,NMAT*(i-1)+1:NMAT*i) = W0;

            GAMMA(i) = rand;

        end 
        
       
    
    case 3
        disp('Heisenberg spin chain')
        
        for i=1:NV
            
            W0 = rand(NMAT,NMAT);
            W0 = W0-W0';
            W0 = W0/norm(W0);
            X0(:,NMAT*(i-1)+1:NMAT*i) = W0;
            
            
        end 
        
    GAMMA = ones(NV,1);
    
    case 4
        disp('Toeplitz inverse eigenvalue problem')
        
        if NV~=1
            error('NV must be 1');
        end
        
        %Centrosymmetry
         I = zeros(NMAT);
         I(1: NMAT+1 : NMAT^2)=1;
         J = flip(I, 2);
            
        W0 = rand(NMAT,NMAT);
        %Random dyagonal starting value
        %W0 = diag(rand(1,NMAT));
        %Random symmetric starting value
        %W0 = W0+W0';
        %Random centrosymmetric starting value
        W0 = J*W0*J+W0;
     
        %Examples
        %W0 = [0.1336 0 0 0.5669;0 -0.1336 0.378 0;0 0.378 -.1336 0;0.5669 0 0 0.1336]; %(Unstable centrosymmetric periodic orbit)
        %W0 = [1 1 -3;1 -2 1;-3 1 1];  %(Unstable centrosymmetric periodic orbit) 
        
        X0 = W0;
    
    case 5
        disp('Periodic Toda flow')
        
        if NV~=1
            error('NV must be 1');
        end
        
        D0 = rand(NMAT,1);
        D1 = rand(NMAT-1,1);
        D2 = rand;

        W0 = diag(D0) + diag(D1,1)  +  (diag(D1,-1)) + diag(D2,NMAT-1) + diag(D2,-NMAT+1);

        X0 = W0;
        
     case 5.1
        disp('Generalized Toda flow')
        
        if NV~=1
            error('NV must be 1');
        end
        
        W0 = rand(NMAT,NMAT);
        W0 = W0 + W0';
        W0 = W0/norm(W0);
        
        X0 = W0;    
        
        
    case 6
        disp('Rigid body')
        
        if NV~=1
            error('NV must be 1');
        end
        
        %Inertia tensor
        J = diag(rand(NMAT,1));
        
        W0 = rand(NMAT,NMAT);
        W0 = W0-W0';
        W0 = W0/norm(W0);
        
        X0 = W0;
              
    otherwise
        error('no equation');
end

end