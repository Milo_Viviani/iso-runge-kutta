function C = mu2(A,B,NV,NMAT)

% persistent sizej
% 
% if isempty(sizej)
    
    sizej = size(A,1)/NMAT;
    
%end
for i=1:NV
    
    for j=1:sizej
        %Here transpose the first factor if the vector field is
        %Hamiltonian
        C(NMAT*(j-1)+1:NMAT*j,NMAT*(i-1)+1:NMAT*i) = A(NMAT*(j-1)+1:NMAT*j,NMAT*(i-1)+1:NMAT*i)'*B(NMAT*(j-1)+1:NMAT*j,NMAT*(i-1)+1:NMAT*i);
        
    end
        
end
