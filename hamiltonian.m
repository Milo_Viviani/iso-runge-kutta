%Hamiltonian function

function H=hamiltonian(X,COR,GAMMA,J,NV,NMAT,eqn)

H = 0;

switch eqn
    
    case 1
        %disp('Vorticity Euler equations on the sphere')
            
        X = X - COR;
        H = norm(trace(discrete_lapl_inv(X,NMAT)*X));
       % 16*pi/NMAT^3*
    case 2
        %disp('Point vortices on a sphere')
 
        for i=1:NV
            for j=1:i-1
        
                H = H - GAMMA(i)*GAMMA(j)*log(1-0.5*(trace(X(:,NMAT*(j-1)+1:NMAT*j)'*X(:,NMAT*(i-1)+1:NMAT*i))));
   
            end
        end
    
    case 3
        %disp('Heisenberg spin chain')
        
        for i=1:NV-1
        
            H = H + real(trace(X(:,NMAT*(i-1)+1:NMAT*i)'*X(:,NMAT*i+1:NMAT*(i+1))));
   
        end

            H = H + real(trace(X(:,NMAT*(NV-1)+1:NMAT*NV)'*X(:,1:NMAT)));

    case 4
        %disp('Toeplitz inverse eigenvalue problem')
        
        H = 0;
       
    case 5
        %disp('Periodic Toda flow')
        
        H = 2*trace(X^2);
        
   case 5.1
        %disp('Generalized Toda flow')
        
        H = 2*trace(X^2);
       
    case 6
        %disp('Rigid body')
 
        H = .5*trace(sylvester(J/2,J/2,X)'*X);
        
     case 7
        %disp('Borel algebra')

         H = 0.5*trace(X'*X);
        
    otherwise
        error('no equation');
end

end